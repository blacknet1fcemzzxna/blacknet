/*
 * Copyright (c) 2018-2019 Pavel Vasin
 * Copyright (c) 2019 Blacknet Team
 *
 * Licensed under the Jelurida Public License version 1.1
 * for the Blacknet Public Blockchain Platform (the "License");
 * you may not use this file except in compliance with the License.
 * See the LICENSE.txt file at the top-level directory of this distribution.
 */

package ninja.blacknet.api

import io.ktor.application.ApplicationCall
import io.ktor.response.respond
import io.ktor.routing.Route
import kotlinx.serialization.Serializable
import ninja.blacknet.core.Staker
import ninja.blacknet.crypto.PrivateKey
import ninja.blacknet.crypto.PublicKey
import ninja.blacknet.ktor.requests.Request
import ninja.blacknet.ktor.requests.get
import ninja.blacknet.ktor.requests.post

fun Route.staking() {
    @Serializable
    class StartStaking(
            val mnemonic: PrivateKey
    ) : Request {
        override suspend fun handle(call: ApplicationCall): Unit {
            val privateKey = mnemonic
            return call.respond(Staker.startStaking(privateKey).toString())
        }
    }

    post(StartStaking.serializer(), "/api/v2/startstaking")

    @Serializable
    class StopStaking(
            val mnemonic: PrivateKey
    ) : Request {
        override suspend fun handle(call: ApplicationCall): Unit {
            val privateKey = mnemonic
            return call.respond(Staker.stopStaking(privateKey).toString())
        }
    }

    post(StopStaking.serializer(), "/api/v2/stopstaking")

    @Serializable
    class IsStaking(
            val mnemonic: PrivateKey
    ) : Request {
        override suspend fun handle(call: ApplicationCall): Unit {
            val privateKey = mnemonic
            return call.respond(Staker.isStaking(privateKey).toString())
        }
    }

    post(IsStaking.serializer(), "/api/v2/isstaking")

    @Serializable
    class Staking(
            val address: PublicKey? = null
    ) : Request {
        override suspend fun handle(call: ApplicationCall): Unit {
            val publicKey = address
            return call.respondJson(StakingInfo.serializer(), Staker.info(publicKey))
        }
    }

    get(Staking.serializer(), "/api/v2/staking/{address?}")
}
